<?xml version="1.0" encoding="UTF-8"?>
<StyledLayerDescriptor version="1.0.0" xmlns="http://www.opengis.net/sld" xmlns:ogc="http://www.opengis.net/ogc"
  xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
  xsi:schemaLocation="http://www.opengis.net/sld http://schemas.opengis.net/sld/1.0.0/StyledLayerDescriptor.xsd">
  <NamedLayer>
   
    <UserStyle>      
      <FeatureTypeStyle>
        
   
        
     
<!-- ================================ -->	
<!--  PointSymbolizer                 -->	
<!-- ================================ -->	
      

        <Rule>
          <!--MinScaleDenominator>300000</MinScaleDenominator-->   

          <PointSymbolizer>
            <Graphic>
              <Mark>
                <WellKnownName>circle</WellKnownName>
                <Fill>
                  <CssParameter name="fill">
                    <ogc:PropertyName>color_hex</ogc:PropertyName>
                  </CssParameter>
                  <CssParameter name="fill-opacity">1</CssParameter>
                </Fill>

                <Stroke>
                   <CssParameter name="stroke">#FFFFFF</CssParameter>

                  <CssParameter name="stroke-width">1</CssParameter>
                </Stroke>


              </Mark>
              <Size>5</Size>
            </Graphic>
          </PointSymbolizer>

        </Rule>
        
<!-- ================================ -->	
<!--  TextSymbolizer                 -->	
<!-- ================================ -->	
              
        <Rule>
          <!--MaxScaleDenominator>32000</MaxScaleDenominator-->
          <!--TextSymbolizer>
            <Label>
              <ogc:PropertyName>address</ogc:PropertyName>
            </Label>
            <Font>
              <CssParameter name="font-family">Arial</CssParameter>
              <CssParameter name="font-weight">Bold</CssParameter>
              <CssParameter name="font-size">14</CssParameter>
            </Font>

            <LabelPlacement>
              <PointPlacement>
                <AnchorPoint>
                  <AnchorPointX>0.5</AnchorPointX>
                  <AnchorPointY>0.5</AnchorPointY>
                </AnchorPoint>
                <Displacement>
                  <DisplacementX>0</DisplacementX>
                  <DisplacementY>-10</DisplacementY>
                </Displacement>
              </PointPlacement>
            </LabelPlacement>
            
            <Halo>
              <Radius>
                <ogc:Literal>2</ogc:Literal>
              </Radius>
              <Fill>
                <CssParameter name="fill">#FFFFFF</CssParameter>
              </Fill>
            </Halo>
            <Fill>
              <CssParameter name="fill">#000000</CssParameter>
            </Fill>
            
            <VendorOption name="conflictResolution">false</VendorOption>
            <VendorOption name="goodnessOfFit">0</VendorOption>
            
          </TextSymbolizer-->
        </Rule>
        
       
      </FeatureTypeStyle>
    </UserStyle>
  </NamedLayer>
</StyledLayerDescriptor>