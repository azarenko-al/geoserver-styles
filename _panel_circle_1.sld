<?xml version="1.0" encoding="UTF-8"?>
<StyledLayerDescriptor version="1.0.0" xmlns="http://www.opengis.net/sld" xmlns:ogc="http://www.opengis.net/ogc"
  xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
  xsi:schemaLocation="http://www.opengis.net/sld http://schemas.opengis.net/sld/1.0.0/StyledLayerDescriptor.xsd">
  <NamedLayer>
    <Name>poi</Name>
    <UserStyle>
      <Name>poi</Name>
      <Title>Points of interest</Title>
      <Abstract>Manhattan points of interest</Abstract>
      <FeatureTypeStyle>
        
        
<!-- ================================ -->	
<!--  tail                                -->	
<!-- ================================ -->	

      

<!-- ================================ -->	

        <Rule>
         

          <PointSymbolizer>
            <Graphic>
              <Mark>
                <WellKnownName>circle</WellKnownName>
                <Fill>
                  <CssParameter name="fill">
                       <ogc:PropertyName>color_hex</ogc:PropertyName>
		  		   </CssParameter>
                  <CssParameter name="fill-opacity">0.8</CssParameter>
                </Fill>

              

              </Mark>
              <Size>50</Size>
            </Graphic>
          </PointSymbolizer>

        </Rule>

<!-- ================================ -->	

      


       
      </FeatureTypeStyle>
    </UserStyle>
  </NamedLayer>
</StyledLayerDescriptor>