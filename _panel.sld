<?xml version="1.0" encoding="UTF-8"?>
<StyledLayerDescriptor version="1.0.0" xmlns="http://www.opengis.net/sld" xmlns:ogc="http://www.opengis.net/ogc"
  xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
  xsi:schemaLocation="http://www.opengis.net/sld http://schemas.opengis.net/sld/1.0.0/StyledLayerDescriptor.xsd">
  <NamedLayer>
    <Name>poi</Name>
    <UserStyle>
      <Name>poi</Name>
      <Title>Points of interest</Title>
      <Abstract>Manhattan points of interest</Abstract>
      <FeatureTypeStyle>

<!-- ================================ -->	

        <Rule>
          <MinScaleDenominator>200000</MinScaleDenominator>   

          <PointSymbolizer>
            <Graphic>
              <Mark>
                <WellKnownName>circle</WellKnownName>
                <Fill>
                  <CssParameter name="fill">
                       <ogc:PropertyName>color_hex</ogc:PropertyName>
		  </CssParameter>
                  <CssParameter name="fill-opacity">1.0</CssParameter>
                </Fill>

                <Stroke>
                  <SvgParameter name="stroke">#FF0000</SvgParameter>
                  <SvgParameter name="stroke-width">0.1</SvgParameter>
                </Stroke>


              </Mark>
              <Size>10</Size>
            </Graphic>
          </PointSymbolizer>

        </Rule>

<!-- ================================ -->	
<!--
        <Rule>
          <MaxScaleDenominator>200000</MaxScaleDenominator>   

          <PointSymbolizer>
            <Graphic>
              <Mark>
                <WellKnownName>circle</WellKnownName>
                <Fill>
                  <CssParameter name="fill">
                       <ogc:PropertyName>color_hex</ogc:PropertyName>
 
		 </CssParameter>
                  <CssParameter name="fill-opacity">1.0</CssParameter>
                </Fill>
                <Stroke>
                  <SvgParameter name="stroke">#FF0000</SvgParameter>
                  <SvgParameter name="stroke-width">0.1</SvgParameter>
                </Stroke>


              </Mark>
              <Size>30</Size>
            </Graphic>
          </PointSymbolizer>

        </Rule>
        
        -->

<!-- ================================ -->	

        <Rule>
         <MaxScaleDenominator>200000</MaxScaleDenominator>   

          <Name>Single symbol</Name>
          <PointSymbolizer>
            <Graphic>
              <ExternalGraphic>
                <!--OnlineResource xlink:type="simple" xlink:href="Arrow-W38-4.svg " /-->
                <OnlineResource xlink:type="simple" xlink:href="Arrow-New-012.svg" />
                
                
                
                <Format>image/svg+xml</Format>
              </ExternalGraphic>

              <!--Mark>
                <WellKnownName>shape://oarrow</WellKnownName>
                <Fill>
                  <SvgParameter name="fill">#FF0000</SvgParameter>
                </Fill>
                <Stroke>
                  <SvgParameter name="stroke">#FF0000</SvgParameter>
                  <SvgParameter name="stroke-width">0.5</SvgParameter>
                </Stroke>
              </Mark-->
              <Size>40</Size>

	 <Rotation>	
          <ogc:PropertyName>azimuth</ogc:PropertyName>
        </Rotation>

            </Graphic>
          </PointSymbolizer>
          
 		<PointSymbolizer>
            <Graphic>
              <Mark>
                <Fill>
                  <CssParameter name="fill">
                    	<ogc:PropertyName>color_hex</ogc:PropertyName>
                  </CssParameter>
                  <CssParameter name="fill-opacity">1</CssParameter>
                </Fill>
              </Mark>
              <Size>50</Size>
            </Graphic>
            <VendorOption name="composite">source-in</VendorOption>
          </PointSymbolizer>
          
        </Rule>



       
      </FeatureTypeStyle>
    </UserStyle>
  </NamedLayer>
</StyledLayerDescriptor>