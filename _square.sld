<?xml version="1.0" encoding="UTF-8"?>
<StyledLayerDescriptor version="1.0.0" xmlns="http://www.opengis.net/sld" xmlns:ogc="http://www.opengis.net/ogc"
  xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
  xsi:schemaLocation="http://www.opengis.net/sld http://schemas.opengis.net/sld/1.0.0/StyledLayerDescriptor.xsd">
  <NamedLayer>
    <Name>poi</Name>
    <UserStyle>
      <Name>poi</Name>
      <Title>Points of interest</Title>
      <Abstract>Manhattan points of interest</Abstract>
      <FeatureTypeStyle>

<!-- ================================ -->	

        <Rule>
         

          <PointSymbolizer>
            <Graphic>
              <Mark>
                <WellKnownName>square</WellKnownName>
                <Fill>
                  <CssParameter name="fill">
                       <ogc:PropertyName>color_hex</ogc:PropertyName>
		  			</CssParameter>
                  <CssParameter name="fill-opacity">1.0</CssParameter>
                </Fill>

                <!--
                <Stroke>
                  <SvgParameter name="stroke">#FF0000</SvgParameter>
                  <SvgParameter name="stroke-width">0.1</SvgParameter>
                </Stroke> -->


              </Mark>
              <Size>20</Size>
              
              <Displacement>
              	<DisplacementX><ogc:PropertyName>offset_x</ogc:PropertyName></DisplacementX>
                <DisplacementY><ogc:PropertyName>offset_y</ogc:PropertyName></DisplacementY>
              </Displacement>
              
              <Rotation>	
                <ogc:PropertyName>azimuth</ogc:PropertyName>
              </Rotation>
              
            </Graphic>
          </PointSymbolizer>

        </Rule>

        
<!-- ================================ -->	
         <Rule>
         

          <PointSymbolizer>
            <Graphic>
              <Mark>
                <WellKnownName>circle</WellKnownName>
                <Fill>
                  <CssParameter name="fill">#FF0000</CssParameter>
                  
                </Fill>

                <!--
                <Stroke>
                  <SvgParameter name="stroke">#FF0000</SvgParameter>
                  <SvgParameter name="stroke-width">0.1</SvgParameter>
                </Stroke> -->


              </Mark>
              <Size>5</Size>
                           
              
            </Graphic>
          </PointSymbolizer>

        </Rule>    

        
        
        <Rule>            
      
        <PointSymbolizer>
               <Graphic>
                 <Mark>
                   <WellKnownName>shape://carrow</WellKnownName>
                   <Fill>
                     <CssParameter name="fill">#AAAAAA</CssParameter>
                   </Fill>
                   <Stroke/>
                 </Mark>
              <Size>30</Size>
                 
                 <Rotation>	
                   <ogc:PropertyName>azimuth_</ogc:PropertyName>
                 </Rotation>

            </Graphic>
          </PointSymbolizer>        


         </Rule>    

<!-- ================================ -->	

   


       
      </FeatureTypeStyle>
    </UserStyle>
  </NamedLayer>
</StyledLayerDescriptor>