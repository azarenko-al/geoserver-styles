<?xml version="1.0" encoding="UTF-8"?>
<StyledLayerDescriptor version="1.0.0" xmlns="http://www.opengis.net/sld" xmlns:ogc="http://www.opengis.net/ogc"
  xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
  xsi:schemaLocation="http://www.opengis.net/sld http://schemas.opengis.net/sld/1.0.0/StyledLayerDescriptor.xsd">
  <NamedLayer>

    <UserStyle>
    
      <FeatureTypeStyle>

<!-- ================================ -->	
<!-- ================================ -->	
<!-- ================================ -->	

        <Rule>
         <!--MaxScaleDenominator>200000</MaxScaleDenominator-->   

         
          <PointSymbolizer>
            <Graphic>
              <ExternalGraphic>
                <!--OnlineResource xlink:type="simple" xlink:href="Arrow-W38-4.svg " /-->
                <OnlineResource xlink:type="simple" xlink:href="Arrow-New-012.svg" />
                
                
                
                <Format>image/svg+xml</Format>
              </ExternalGraphic>
            
              <Size>40</Size>

	 <Rotation>	
          <ogc:PropertyName>azimuth</ogc:PropertyName>
        </Rotation>

            </Graphic>
          </PointSymbolizer>
          
 		<PointSymbolizer>
            <Graphic>
              <Mark>
                <Fill>
                  <CssParameter name="fill">
                    	<ogc:PropertyName>color_hex</ogc:PropertyName>
                  </CssParameter>
                  <CssParameter1 name="fill-opacity">1</CssParameter1>
                </Fill>
              </Mark>
              <Size>40</Size>
            </Graphic>
            <VendorOption name="composite">source-in</VendorOption>
          </PointSymbolizer>
          
        </Rule>



       
      </FeatureTypeStyle>
    </UserStyle>
  </NamedLayer>
</StyledLayerDescriptor>