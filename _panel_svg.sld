<?xml version="1.0" encoding="UTF-8"?>
<StyledLayerDescriptor version="1.0.0" xmlns="http://www.opengis.net/sld" xmlns:ogc="http://www.opengis.net/ogc"
                       xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
                       xsi:schemaLocation="http://www.opengis.net/sld http://schemas.opengis.net/sld/1.0.0/StyledLayerDescriptor.xsd">
  <NamedLayer>
      <UserStyle>
      
      <FeatureTypeStyle>


<!-- === M:9 ============= -->	


<!-- === M:10 ============= -->	

        <Rule>
         <MaxScaleDenominator>700000</MaxScaleDenominator>   
         <MinScaleDenominator>300000</MinScaleDenominator>
         
          <PointSymbolizer>
            <Graphic>
              <Mark>                
                <WellKnownName>circle</WellKnownName>   
                <Fill>
                  <CssParameter name="fill-opacity">0</CssParameter>
                </Fill>              
                <Stroke>
                  <CssParameter name="stroke">#FF0000</CssParameter>               
                  <CssParameter name="stroke-width">2</CssParameter>
                </Stroke>

              </Mark>
              <Size>20</Size>
            </Graphic>
          </PointSymbolizer>


		  <!--PointSymbolizer>
            <Graphic>
              <Mark>
                              
                <WellKnownName>wkt://LINESTRING (0 -2, 0 2)</WellKnownName>
              
				<Stroke>
				  <CssParameter name="stroke">#FF0000</CssParameter>
				  <CssParameter name="stroke-width">3</CssParameter>								  
				</Stroke>					

              </Mark>
              <Size>8</Size>
              
               <Rotation>	
                <ogc:PropertyName>azimuth</ogc:PropertyName>
              </Rotation>              
            </Graphic>
          </PointSymbolizer-->


          <PointSymbolizer>
            <Graphic>
              <ExternalGraphic>
                <ogc:PropertyName>href</ogc:PropertyName>
                <Format>image/svg+xml</Format>
              </ExternalGraphic>

              <Size>20</Size>

              <Rotation>	
                <ogc:PropertyName>azimuth</ogc:PropertyName>
              </Rotation>

            </Graphic>
          </PointSymbolizer>

        </Rule>

<!-- === M:11 ============= -->	
      
        <Rule>
         <MaxScaleDenominator>300000</MaxScaleDenominator>   
         <MinScaleDenominator>200000</MinScaleDenominator>   

          <PointSymbolizer>
            <Graphic>
              <Mark>                
                <WellKnownName>circle</WellKnownName>   
                <Fill>
                  <CssParameter name="fill-opacity">0</CssParameter>
                </Fill>              
                <Stroke>
                  <CssParameter name="stroke">#FF0000</CssParameter>               
                  <CssParameter name="stroke-width">2</CssParameter>
                </Stroke>

              </Mark>
              <Size>24</Size>
            </Graphic>
          </PointSymbolizer>

        <!-- =========================== -->	

		  <!--PointSymbolizer>
            <Graphic>
              <Mark>               
                
                <WellKnownName>wkt://LINESTRING (0 -2, 0 2)</WellKnownName>
              
				<Stroke>
				  <CssParameter name="stroke">#FF0000</CssParameter>
				  <CssParameter name="stroke-width">5</CssParameter>								  
				</Stroke>					

              </Mark>
              <Size>12</Size>
              
               <Rotation>	
                <ogc:PropertyName>azimuth</ogc:PropertyName>
              </Rotation>              
            </Graphic>
          </PointSymbolizer-->
        
          <PointSymbolizer>
            <Graphic>
              <ExternalGraphic>
                <OnlineResource xlink:type="simple" xlink:href="Arrow-New-0111-Black.svg" />
                <Format>image/svg+xml</Format>
              </ExternalGraphic>

              <Size>24</Size>

              <Rotation>	
                <ogc:PropertyName>azimuth</ogc:PropertyName>
              </Rotation>

            </Graphic>
          </PointSymbolizer>            
        </Rule>


<!-- === M:12 ============= -->	

       <Rule>
          <MaxScaleDenominator>200000</MaxScaleDenominator>   
          <MinScaleDenominator>100000</MinScaleDenominator>   
        
          <PointSymbolizer>
            <Graphic>
              <Mark>                
                <WellKnownName>circle</WellKnownName>   
                <Fill>
                  <CssParameter name="fill-opacity">0</CssParameter>
                </Fill>              
                <Stroke>
                  <CssParameter name="stroke">#FF0000</CssParameter>               
                  <CssParameter name="stroke-width">2</CssParameter>
                </Stroke>

              </Mark>
              <Size>26</Size>
            </Graphic>
          </PointSymbolizer>

        <!-- =========================== -->	

		  <!--PointSymbolizer>
            <Graphic>
              <Mark>
                              
                <WellKnownName>wkt://LINESTRING (0 -2, 0 2)</WellKnownName>
              
				<Stroke>
				  <CssParameter name="stroke">#FF0000</CssParameter>
				  <CssParameter name="stroke-width">5</CssParameter>								  
				</Stroke>					

              </Mark>
              <Size>14</Size>
              
               <Rotation>	
                <ogc:PropertyName>azimuth</ogc:PropertyName>
              </Rotation>              
            </Graphic>
          </PointSymbolizer-->
       

          <PointSymbolizer>
            <Graphic>
              <ExternalGraphic>
                <OnlineResource xlink:type="simple" xlink:href="Arrow-New-0111-Black.svg" />
                <Format>image/svg+xml</Format>
              </ExternalGraphic>

              <Size>26</Size>

              <Rotation>	
                <ogc:PropertyName>azimuth</ogc:PropertyName>
              </Rotation>

            </Graphic>
          </PointSymbolizer>            
        </Rule>
 

<!-- === M:13 ============= -->	
        <Rule>
          <MaxScaleDenominator>100000</MaxScaleDenominator>   
          <MinScaleDenominator>50000</MinScaleDenominator>   


          <PointSymbolizer>
            <Graphic>
              <Mark>                
                <WellKnownName>circle</WellKnownName>   
                <Fill>
                  <CssParameter name="fill-opacity">0</CssParameter>
                </Fill>              
                <Stroke>
                  <CssParameter name="stroke">#FF0000</CssParameter>               
                  <CssParameter name="stroke-width">2</CssParameter>
                </Stroke>

              </Mark>
              <Size>28</Size>
            </Graphic>
          </PointSymbolizer>

        <!-- =========================== -->	

		  <!--PointSymbolizer>
            <Graphic>
              <Mark>
                              
                <WellKnownName>wkt://LINESTRING (0 -2, 0 2)</WellKnownName>
              
				<Stroke>
				  <CssParameter name="stroke">#FF0000</CssParameter>
				  <CssParameter name="stroke-width">5</CssParameter>								  
				</Stroke>					

              </Mark>
              <Size>14</Size>
              
               <Rotation>	
                <ogc:PropertyName>azimuth</ogc:PropertyName>
              </Rotation>              
            </Graphic>
          </PointSymbolizer-->

          <PointSymbolizer>
            <Graphic>
              <ExternalGraphic>
                <OnlineResource xlink:type="simple" xlink:href="Arrow-New-0111-Black.svg" />
                <Format>image/svg+xml</Format>
              </ExternalGraphic>

              <Size>28</Size>

              <Rotation>	
                <ogc:PropertyName>azimuth</ogc:PropertyName>
              </Rotation>

            </Graphic>
          </PointSymbolizer>     
        </Rule>


<!-- === M:14 ============= -->	
        <Rule>
          <MaxScaleDenominator>50000</MaxScaleDenominator>   
          <MinScaleDenominator>25000</MinScaleDenominator>   

          <PointSymbolizer>
            <Graphic>
              <Mark>                
                <WellKnownName>circle</WellKnownName>   
                <Fill>
                  <CssParameter name="fill-opacity">0</CssParameter>
                </Fill>              
                <Stroke>
                  <CssParameter name="stroke">#FF0000</CssParameter>               
                  <CssParameter name="stroke-width">2</CssParameter>
                </Stroke>

              </Mark>
              <Size>30</Size>
            </Graphic>
          </PointSymbolizer>

        <!-- =========================== -->	

		  <!--PointSymbolizer>
            <Graphic>
              <Mark>                              
                <WellKnownName>wkt://LINESTRING (0 -2, 0 2)</WellKnownName>
              
				<Stroke>
				  <CssParameter name="stroke">#FF0000</CssParameter>
				  <CssParameter name="stroke-width">5</CssParameter>								  
				</Stroke>					

              </Mark>
              <Size>14</Size>
              
               <Rotation>	
                <ogc:PropertyName>azimuth</ogc:PropertyName>
              </Rotation>              
            </Graphic>
          </PointSymbolizer-->

          <PointSymbolizer>
            <Graphic>
              <ExternalGraphic>
                <OnlineResource xlink:type="simple" xlink:href="Arrow-New-0111-Black.svg" />
                <Format>image/svg+xml</Format>
              </ExternalGraphic>

              <Size>30</Size>

              <Rotation>	
                <ogc:PropertyName>azimuth</ogc:PropertyName>
              </Rotation>

            </Graphic>
          </PointSymbolizer>     
        </Rule>

<!-- === M:15 ============= -->	
        <Rule>
		  <MaxScaleDenominator>25000</MaxScaleDenominator>   
          <MinScaleDenominator>12500</MinScaleDenominator>   

          <PointSymbolizer>
            <Graphic>
              <Mark>                
                <WellKnownName>circle</WellKnownName>   
                <Fill>
                  <CssParameter name="fill-opacity">0</CssParameter>
                </Fill>              
                <Stroke>
                  <CssParameter name="stroke">#FF0000</CssParameter>               
                  <CssParameter name="stroke-width">2</CssParameter>
                </Stroke>

              </Mark>
              <Size>32</Size>
            </Graphic>
          </PointSymbolizer>

        <!-- =========================== -->	

		  <PointSymbolizer>
            <Graphic>
              <Mark>          
                
                <WellKnownName>wkt://LINESTRING (0 -2, 0 2)</WellKnownName>
              
				<Stroke>
				  <CssParameter name="stroke">#FF0000</CssParameter>
				  <CssParameter name="stroke-width">5</CssParameter>								  
				</Stroke>					

              </Mark>
              <Size>14</Size>
              
               <Rotation>	
                <ogc:PropertyName>azimuth</ogc:PropertyName>
              </Rotation>              
            </Graphic>
          </PointSymbolizer>

          <!--PointSymbolizer>
            <Graphic>
              <ExternalGraphic>
                <OnlineResource xlink:type="simple" xlink:href="Arrow-New-0111-Black.svg" />
                <Format>image/svg+xml</Format>
              </ExternalGraphic>

              <Size>32</Size>

              <Rotation>	
                <ogc:PropertyName>azimuth</ogc:PropertyName>
              </Rotation>

            </Graphic>
          </PointSymbolizer-->     
        </Rule>

<!-- === M:16 ============= -->	
        
        <Rule>
          <MaxScaleDenominator>12500</MaxScaleDenominator>   
          <MinScaleDenominator>6250</MinScaleDenominator>   

          <PointSymbolizer>
            <Graphic>
              <Mark>                
                <WellKnownName>circle</WellKnownName>   
                <Fill>
                  <CssParameter name="fill-opacity">0</CssParameter>
                </Fill>              
                <Stroke>
                  <CssParameter name="stroke">#FF0000</CssParameter>               
                  <CssParameter name="stroke-width">2</CssParameter>
                </Stroke>

              </Mark>
              <Size>34</Size>
            </Graphic>
          </PointSymbolizer>

        <!-- =========================== -->	

		  <PointSymbolizer>
            <Graphic>
              <Mark>
                              
                <WellKnownName>wkt://LINESTRING (0 -2, 0 2)</WellKnownName>
              
				<Stroke>
				  <CssParameter name="stroke">#FF0000</CssParameter>
				  <CssParameter name="stroke-width">5</CssParameter>								  
				</Stroke>					

              </Mark>
              <Size>14</Size>
              
               <Rotation>	
                <ogc:PropertyName>azimuth</ogc:PropertyName>
              </Rotation>              
            </Graphic>
          </PointSymbolizer>

         <!--PointSymbolizer>
            <Graphic>
              <ExternalGraphic>
                <OnlineResource xlink:type="simple" xlink:href="Arrow-New-0111-Black.svg" />
                <Format>image/svg+xml</Format>
              </ExternalGraphic>

              <Size>34</Size>

              <Rotation>	
                <ogc:PropertyName>azimuth</ogc:PropertyName>
              </Rotation>

            </Graphic>
          </PointSymbolizer-->   
        </Rule>

<!-- === M:17 ============= -->	
        <Rule>
          <MaxScaleDenominator>6250</MaxScaleDenominator>   
          <MinScaleDenominator>3125</MinScaleDenominator>   


          <PointSymbolizer>
            <Graphic>
              <Mark>                
                <WellKnownName>circle</WellKnownName>   
                <Fill>
                  <CssParameter name="fill-opacity">0</CssParameter>
                </Fill>              
                <Stroke>
                  <CssParameter name="stroke">#FF0000</CssParameter>               
                  <CssParameter name="stroke-width">2</CssParameter>
                </Stroke>

              </Mark>
              <Size>38</Size>
            </Graphic>
          </PointSymbolizer>

        <!-- =========================== -->	

		  <PointSymbolizer>
            <Graphic>
              <Mark>
                               
                <WellKnownName>wkt://LINESTRING (0 -2, 0 2)</WellKnownName>
              
				<Stroke>
				  <CssParameter name="stroke">#FF0000</CssParameter>
				  <CssParameter name="stroke-width">5</CssParameter>								  
				</Stroke>					

              </Mark>
              <Size>24</Size>
              
               <Rotation>	
                <ogc:PropertyName>azimuth</ogc:PropertyName>
              </Rotation>              
            </Graphic>
          </PointSymbolizer>

		  <!--PointSymbolizer>
            <Graphic>
              <ExternalGraphic>
                <OnlineResource xlink:type="simple" xlink:href="Arrow-New-0111-Black.svg" />
                <Format>image/svg+xml</Format>
              </ExternalGraphic>

              <Size>38</Size>

              <Rotation>	
                <ogc:PropertyName>azimuth</ogc:PropertyName>
              </Rotation>

            </Graphic>
          </PointSymbolizer-->          
          
        </Rule>
        

<!-- === M:18 =========== -->	
        
        <Rule>
          <MaxScaleDenominator>3125</MaxScaleDenominator>   
        
          <PointSymbolizer>
            <Graphic>
              <Mark>                
                <WellKnownName>circle</WellKnownName>   
                <Fill>
                  <CssParameter name="fill-opacity">0</CssParameter>
                </Fill>              
                <Stroke>
                  <CssParameter name="stroke">#FF0000</CssParameter>               
                  <CssParameter name="stroke-width">2</CssParameter>
                </Stroke>

              </Mark>
              <Size>46</Size>
            </Graphic>
          </PointSymbolizer>

        <!-- =========================== -->	

		  <PointSymbolizer>
            <Graphic>
              <Mark>
                               
                <WellKnownName>wkt://LINESTRING (0 -2, 0 2)</WellKnownName>
              
				<Stroke>
				  <CssParameter name="stroke">#FF0000</CssParameter>
				  <CssParameter name="stroke-width">5</CssParameter>								  
				</Stroke>					

              </Mark>
              <Size>24</Size>
              
               <Rotation>	
                <ogc:PropertyName>azimuth</ogc:PropertyName>
              </Rotation>              
            </Graphic>
          </PointSymbolizer>

           <!--PointSymbolizer>
            <Graphic>
              <ExternalGraphic>
                <OnlineResource xlink:type="simple" xlink:href="Arrow-New-0111-Black.svg" />
                <Format>image/svg+xml</Format>
              </ExternalGraphic>

              <Size>46</Size>

              <Rotation>	
                <ogc:PropertyName>azimuth</ogc:PropertyName>
              </Rotation>

            </Graphic>
          </PointSymbolizer-->    
        </Rule>      

      </FeatureTypeStyle>
    </UserStyle>
  </NamedLayer>
</StyledLayerDescriptor>