<?xml version="1.0" encoding="UTF-8"?>
<StyledLayerDescriptor version="1.0.0" xmlns="http://www.opengis.net/sld" xmlns:ogc="http://www.opengis.net/ogc"
                       xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
                       xsi:schemaLocation="http://www.opengis.net/sld http://schemas.opengis.net/sld/1.0.0/StyledLayerDescriptor.xsd">
  <NamedLayer>
    <Name>poi</Name>
    <UserStyle>
      
      <FeatureTypeStyle>

        <!-- ================================ -->	

 		 <!--Rule>
        
           <PointSymbolizer>
            <Graphic>
              <Mark>
                <WellKnownName>circle</WellKnownName>                
                
                <Fill>
                  <CssParameter name="fill">
                       <ogc:PropertyName>color_hex</ogc:PropertyName>
		  		   </CssParameter>
                  <CssParameter name="fill-opacity">0</CssParameter>
                </Fill>


            <Stroke>
              <CssParameter name="stroke">#FF0000</CssParameter>
              <CssParameter name="stroke-width">10</CssParameter>
			  <CssParameter name="fill-opacity">1</CssParameter>
              
            </Stroke>
                

              </Mark>
              <Size>10</Size>
              
               <Rotation>	
                <ogc:PropertyName>azimuth</ogc:PropertyName>
              </Rotation>

              
            </Graphic>
          </PointSymbolizer>

        </Rule-->
      
        
 		 <Rule>
         
	<!--PointSymbolizer>
       <Graphic>
         <ExternalGraphic>
           <OnlineResource xlink:type="simple" xlink:href="_panel1.png" />
           <Format>image/png</Format>
           
            <Fill>
                  <CssParameter name="fill">
                       <ogc:PropertyName>color_hex</ogc:PropertyName>
		  		   </CssParameter>
                  <CssParameter name="fill-opacity">0</CssParameter>
                </Fill>
         </ExternalGraphic>
         <Size>80</Size>
         <WellKnownName>circle</WellKnownName>
               
      </Graphic>
    </PointSymbolizer-->

           
          <PointSymbolizer>
            <Graphic>
              <Mark>
                <!--WellKnownName>circle</WellKnownName-->
                
                <WellKnownName>wkt://LINESTRING (0 -2, 0 2)</WellKnownName>
                
                <Fill>
                  <CssParameter name="fill">
                       <ogc:PropertyName>color_hex</ogc:PropertyName>
		  		   </CssParameter>
                  <CssParameter name="fill-opacity">0</CssParameter>
                </Fill>


            <Stroke>
              <CssParameter name="stroke">#FF0000</CssParameter>
              <CssParameter name="stroke-width">5</CssParameter>
			  <CssParameter name="fill-opacity">1</CssParameter>
              
            </Stroke>
                

              </Mark>
              <Size>20</Size>
              
               <Rotation>	
                <ogc:PropertyName>azimuth</ogc:PropertyName>
              </Rotation>

              
            </Graphic>
          </PointSymbolizer>

        </Rule>

        <!-- ================================ -->	

        <!--Rule>         

          
          <PointSymbolizer>
            <Graphic>
              <ExternalGraphic>
                <OnlineResource xlink:type="simple" xlink:href="Arrow-New-0111_4.svg" />
                <Format>image/svg+xml</Format>
              </ExternalGraphic>


              <Size>40</Size>
            
              
              <Rotation>	
                <ogc:PropertyName>azimuth</ogc:PropertyName>
              </Rotation>

            </Graphic>
            
          
         
          </PointSymbolizer>          
        </Rule-->





      </FeatureTypeStyle>
    </UserStyle>
  </NamedLayer>
</StyledLayerDescriptor>